﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OOPASU.Domain
{
    public class Attendance
    {
        public Guid Id { get; set; }
        public bool Late { get; set; }


        // Свойства навигации
        public Student Student { get; set; }
        public Lesson Lesson  { get; set; }
    }
}