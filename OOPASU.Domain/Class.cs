﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OOPASU.Domain
{
    public class Class
    {
        public Guid Id { get; set; }
        public int Number { get; set; }

        //Навигация
        public Lesson Lesson { get; set; }

    }
}
