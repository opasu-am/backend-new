﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OOPASU.Domain
{
    public class Teacher
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Father_Name { get; set; }

        // Свойства навигации
        public Lesson Lesson { get; set; }
    }
}
