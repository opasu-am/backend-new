﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OOPASU.Domain
{
    public class Group
    {
        public Guid Id { get; set; }
        public int Number { get; set; }
        public string Faculty { get; set; }

        // Свойства навигации
        public Lesson Lesson { get; set; }
        public List<Student> Students { get; set; }
    }
}
