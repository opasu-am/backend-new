﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OOPASU.Domain
{
    public class Lesson
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string Topic { get; set; }
        public int Number { get; set; }

        // Свойства навигации
        public List<Teacher> Teachers { get; set; }
        public List<Class> Classes { get; set; }
        public Discipline Discipline { get; set; }
        public List<Group> Groups { get; set; }
        public List<Attendance> Attendances { get; set; }
    }
}