﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OOPASU.Domain
{
    public class Discipline
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        //Навигация
        public List<Lesson> Lessons { get; set; }
    }
}