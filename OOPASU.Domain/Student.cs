﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OOPASU.Domain
{
    public class Student
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Father_Name { get; set; }
        public DateTime Birth_Date { get; set; }
        public string Email { get; set; }

        // Свойства навигации
        public List<Teacher> Results { get; set; }
        public List<Attendance> Attendances { get; set; }
        public Group Group { get; set; }
    }
}
